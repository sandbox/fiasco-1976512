INSTALLATION
============

* Install and enable authcache, boost and authcache_boost modules.
* Add the following line to your settings.php:
  $conf['authcache_cache_handler'] = 'authcache_boost_cache_save';
