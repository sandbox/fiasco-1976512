INSTALLATION
============

* Install and enable authcache, varnish and authcache_varnish modules.
* Add the following line to your settings.php:
  $conf['authcache_cache_handler'] = 'authcache_varnish_cache_save';

Configure varnish according to the directions given by Josh Waihi in the
following blog-post.
* http://joshwaihi.com/content/authenticated-page-caching-varnish-drupal

Note that we are using X-Authcache-Key for passing the key from drupal to
varnish and the page callback is under /authcache-varnish-get-key. An example
VCL is included in the file example.vcl.
