import digest;
sub vcl_recv {
  if (req.request == "GET" || req.request == "HEAD") {
    // Check for an authenticated cookie that indicates that the request
    // should forcefully bypass cache this time.
    call req_build_hash;
    if (req.http.X-Varnish-Pass == 'true') {
      call req_clean_hash;
      return (pass);
    }
    if (req.restarts == 0) {
      // Before handling the actual request the authcache-key needs to be
      // determined. This is accomplished by querying the backend on
      // /authcache-varnish-get-key. Upon vcl_deliver the authcacke-key is
      // copied over to the X-Authcache-Key request header and the request is
      // restarted.
      unset req.http.X-Authcache-Key;
      set req.http.X-Original-URL = req.url;
      set req.url = "/authcache-varnish-get-key";

      // Backup cookies and unset the cookie header. We do this because the
      // default vcl_recv returns pass when cookies are present on the request.
      // Note that cookies must be restored before the request is sent to the
      // backend in vcl_hash.
      call req_backup_cookies;
    }
    elseif (req.http.X-Authcache-Key) {
      // When the authcache-key is present on the request, cookies are removed
      // from the normal cookie header. This will allow caching by the default
      // VCL.
      call req_backup_cookies;
    }
  }
}

sub vcl_hash {
  // Restore cookies before request is handled
  call req_restore_cookies;

  // Separate cache bins for js/non-js clients
  if (req.http.Cookie ~ "(^|;)\s*has_js=1\s*($|;)") {
    hash_data("has_js=1");
  }
}

sub vcl_deliver {
  if (req.request == "GET" || req.request == "HEAD") {
    // Process response from authcache-key callback
    if (req.http.X-Original-URL) {
      // Restore the original URL.
      set req.url = req.http.X-Original-URL;
      unset req.http.X-Original-URL;

      // Copy over the X-Authcache-Key header if set
      if (resp.http.X-Authcache-Key) {
        set req.http.X-Authcache-Key = resp.http.X-Authcache-Key;
      }

      return (restart);
    }
  }

  // If the request contains a varnish_pass Cookie, remove it.
  // These cookies are only useful for a single request.
  if (req.http.Cookie ~ 'varnish_pass') {
    set resp.http.Set-Cookie = 'varnish_pass=deleted; Expires=Thu, 01-Jan-1970 00:00:01 GMT; Domain=' + req.http.Host + '; Path=/;';
  }
  // If the user has is receiving a redirect response
  // from a POST request, its possible that the next request
  // should pass even if the next request already exists in
  // cache. We'll set a cookie to indicate that the user's
  // next request shouldn't be cached. So that hackers don't
  // exploit this function as a means for DoS attacks we'll
  // set the value as a hmac hash so that we can validate it
  // in vcl_recv.
  if (req.request == "POST" && resp.http.Location) {
    call req_build_hash;
    set resp.http.Set-Cookie = 'varnish_pass=' + digest.hmac_sha256("<insert_secret_key>", req.http.hash) + '; Domain=' + req.http.Host + '; Path=/;';
    call req_clean_hash;
  }

  // When sending the response back to the browser:
  // 1. Add "Cookie" to Vary header
  set resp.http.Vary = resp.http.Vary + ", Cookie";
  // 2. Remove X-Authcache-Key from Vary header
  set resp.http.Vary = regsub(resp.http.Vary, "(^|,\s*)X-Authcache-Key", "");
  // 3. Remove a "," prefix, if present.
  set resp.http.Vary = regsub(resp.http.Vary, "^,\s*", "");
}

/**
 * Backup cookies into X-Cookie-Backup before the default vcl_recv
 *
 * Call this subroutine from vcl_recv.
 */
sub req_backup_cookies {
  if (req.http.Cookie) {
    set req.http.X-Cookie-Backup = req.http.Cookie;
    unset req.http.Cookie;
  }
}

/**
 * Restore cookies from X-Cookie-Backup after vcl_recv
 *
 * Call this subroutine from vcl_hash.
 */
sub req_restore_cookies {
  if (req.http.X-Cookie-Backup) {
    set req.http.Cookie = req.http.X-Cookie-Backup;
    unset req.http.X-Cookie-Backup;
  }
}

/**
 * Set hash value for request.
 */
sub req_build_hash {
  // Determine the hash to authenticate against.
  if (!req.http.Cookie ~ "SESS") {
    set req.http.hash = client.ip;
  }
  else {
    set req.http.hash = regsuball(req.http.Cookie, "^.*SESS[^=]+=([^; ]+).*$", "\1");
  }

  set req.http.X-Varnish-Pass = 'false';
  // If the varnish_pass Cookie is present we can derive the
  // hmac value also.
  if (req.http.Cookie ~ 'varnish_pass') {
    set req.http.hmac = regsuball(req.http.Cookie, "^.*varnish_pass=([^; ]+).*$", "\1");

    // If the hmac from the request is equal to the evaluated hmac
    // then this request is legit. 
    if (digest.hmac_sha256("<insert_secret_key>", req.http.hash) == req.http.hmac) {
      set req.http.X-Varnish-Pass = 'true';
    }
  }
}

sub req_clean_hash {
  unset req.http.hash;
  unset req.http.hmac;
  unset req.http.X-Varnish-Pass;
}
