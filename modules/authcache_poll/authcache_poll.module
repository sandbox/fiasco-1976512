<?php

/**
 * Implements hook_block_view_MODULE_DELTA_alter().
 */
function authcache_poll_block_view_poll_recent_alter(&$data, $block) {
  if (authcache_page_is_cacheable()) {
    authcache_cancel(t('FIXME: authcache_poll_block_view_poll_recent_alter not implemented yet'));
  }
}

/**
 * Implements hook_node_view_alter().
 *
 * Replace poll form and result view with a span which will be populated with its
 * actual content during the ajax phase.
 */
function authcache_poll_node_view_alter(&$build) {
  $node = $build['#node'];

  if ($node->type == 'poll' && authcache_page_is_cacheable()) {
    if (authcache_get_request_property('ajax')) {
      authcache_ajax_add_command('poll');
      drupal_add_js(drupal_get_path('module', 'authcache_poll') . '/authcache_poll.js');
      if (isset($build['poll_view_voting'])) {
        $build['poll_view_voting'] = array(
          '#markup' => '<span class="authcache-poll" data-nid="' . check_plain($node->nid) . '"></span>',
        );
      }
      if (isset($build['poll_view_results'])) {
        $build['poll_view_results'] = array(
          '#markup' => '<span class="authcache-poll" data-nid="' . check_plain($node->nid) . '"></span>',
        );
      }
    }
    else {
      authcache_cancel(t('Open polls on page and Authcache AJAX is not enabled.'));
    }
  }
}

/**
 * Implements hook_form_FORM_ID_alter(),
 */
function authcache_poll_form_poll_view_voting_alter(&$form, &$form_state) {
  if (authcache_get_request_property('ajax')) {
    $form['vote']['#submit'][] = 'authcache_poll_browsercache_invalidate';
  }
}

/**
 * Implements hook_form_FORM_ID_alter(),
 */
function authcache_poll_form_poll_cancel_form_alter(&$form, &$form_state) {
  if (authcache_get_request_property('ajax')) {
    $form['actions']['submit']['#submit'][] = 'authcache_poll_cache_clear_all';
    $form['actions']['submit']['#submit'][] = 'authcache_poll_browsercache_invalidate';
  }
}

/**
 * Update browser cache after poll-form submission.
 */
function authcache_poll_browsercache_invalidate() {
  authcache_ajax_browsercache_invalidate();
}

/**
 * Invalidate page cache when a vote was cancelled.
 *
 * Most probably this is an oversight in poll.module. While poll_vote() clears
 * the cache, poll_cancel does not. Drupal core does not serve stale caches to
 * anonymous users only because poll stores votes into $_SESSION effectively
 * deactivating page caching.
 */
function authcache_poll_cache_clear_all() {
  cache_clear_all();
}

/**
 * Implements hook_authcache_menu_item_cacheable().
 */
function authcache_poll_authcache_menu_item_cacheable() {
  $items['node/%/votes'] = TRUE;
  $items['node/%/results'] = FALSE;

  return $items;
}

/**
 * Implements hook_authcache_ajax_command().
 */
function authcache_poll_authcache_ajax_command() {
  return array(
    'poll' => array(
      'handler' => 'AuthcachePollGetCommand',
      'handler config' => array(
        // FULL bootstrap required in case custom theming is used
        'bootstrap' => DRUPAL_BOOTSTRAP_FULL,
      ),
      'request' => 'poll',
      'request config' => array(
        'maxage' => 600,
      ),
    ),
  );
}
