<?php
/**
 * @file
 * Defines command handler for ajax-blocks.
 */


/**
 * Command handler for ajax-blocks.
 *
 * FIXME: Due to the fact that blocks are cached as render-array on the server,
 * we need a full bootstrap plus initialized theming layer. Perhaps we find a
 * better way to reduce the bootstrap level by maintaining a authcache-only 
 * version of the block-cache.
 */
class AuthcacheBlockGetCommand extends AuthcacheAjaxCommandHandlerBase {
  /**
   * Render blocks. Grab from cache if available.
   * @param <array> $blocks
   *   [block id] => [block cache id]
   * @see block.module
   */
  function run($op, $blocks) {
    include_once './includes/common.inc';           // drupal_render_cid_parts
    include_once './modules/block/block.module';    // block_load

    global $user, $theme_key;
    $return = array();

    foreach ($blocks as $block_id => $block_cid) {
      list($module, $delta) = explode('-', $block_id, 2);
      $block = block_load($module, $delta);

      $cid = $this->getBlockCacheId($block);

      if ($cid) {
        // Attempt to load block from cache
      }

      // Block is either not cacheable or is missing from cache. Need in order
      // to build the markup.
      drupal_bootstrap(DRUPAL_BOOTSTRAP_FULL);

      $build = reset(_block_get_renderable_array(_block_render_blocks(array($block_id => $block))));
      if (!empty($build['#theme_wrappers'])) {
        $build['#theme_wrappers'] = array_diff($build['#theme_wrappers'], array('block'));
      }

      $return[$block_id] = array(
        'subject' => check_plain($build['#block']->subject),
        'content' => render($build),
      );

      if ($cid) {
        // Set cache
      }
    }

    return $return;
  }

  private function getBlockCacheId($block) {
    global $user;

    if ($block->cache < 0) {
      return;
    }

    $cid_parts[] = $block->module;
    $cid_parts[] = $block->delta;
    $cid_parts = array_merge($cid_parts, drupal_render_cid_parts($block->cache));

    return implode(':', $cid_parts);
  }
}
